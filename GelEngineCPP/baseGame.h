#pragma once

/* The base game that any game must inherit from. */
class BaseGame
{
public:
	virtual ~BaseGame(){};
	/* Initialize the game here by loading all needed resources. */
	virtual void init() = 0;
	/* Invoked when the window is resized. */
	virtual void resized() = 0;
	/* Update game logic. NOTE: Do not render anything here. */
	virtual void update() = 0;
	/* Render's the game. NOTE: Do not place update logic here.*/
	virtual void render() = 0;

};

