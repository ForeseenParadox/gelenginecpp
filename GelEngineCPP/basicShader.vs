#version 120

uniform mat4 viewProjectionMatrix;

attribute vec2 position;
attribute vec4 color;
attribute vec2 texCoord;

varying vec4 v_color;
varying vec2 v_texCoord;

void main()
{
	gl_Position = viewProjectionMatrix * vec4(position,0,1);
	v_color = color;
	v_texCoord = texCoord;
}