#include "bitmap.h"
#include <iostream>

Bitmap::Bitmap(const std::string& path)
{
	SDL_Surface* img = IMG_Load(path.c_str());

	if (img == nullptr)
	{
		std::cerr << "Could not load bitmap at: " << path;
	} else
	{
		SDL_PixelFormat* format = img->format;

		m_width = img->w;
		m_height = img->h;

		void* void_data = img->pixels;

		if (format->format == SDL_PIXELFORMAT_RGB24)
		{
			m_data = new unsigned char[m_width * m_height * 3];
			m_hasAlphaChannel = false;

			for (int i = 0; i < m_width * m_height; i++)
			{
				m_data[i * 3 + 0] = *(((unsigned char*)void_data) + i * 3 + 0);
				m_data[i * 3 + 1] = *(((unsigned char*)void_data) + i * 3 + 1);
				m_data[i * 3 + 2] = *(((unsigned char*)void_data) + i * 3 + 2);
			}
		} else if (format->format == SDL_PIXELFORMAT_ABGR8888)
		{
			m_data = new unsigned char[m_width * m_height * 4];
			m_hasAlphaChannel = true;

			for (int i = 0; i < m_width * m_height; i++)
			{
				m_data[i * 4 + 0] = *(((unsigned char*)void_data) + i * 4 + 0); 
				m_data[i * 4 + 1] = *(((unsigned char*)void_data) + i * 4 + 1); 
				m_data[i * 4 + 2] = *(((unsigned char*)void_data) + i * 4 + 2);
				m_data[i * 4 + 3] = *(((unsigned char*)void_data) + i * 4 + 3);
			}
		} else
		{
			std::cerr << "Unsupported pixel format of: " << format->format << std::endl;
		}

		flipY();

		SDL_FreeSurface(img);
		SDL_FreeFormat(format);
	}
}

Bitmap::Bitmap(int w, int h, bool hasAlphaChannel)
{
	m_width = w;
	m_height = h;

	m_data = new unsigned char[w * h * (hasAlphaChannel ? 4 : 3)];
	m_hasAlphaChannel = hasAlphaChannel;
}

Bitmap::~Bitmap()
{
	delete[] m_data;
}

int Bitmap::getWidth() const
{
	return m_width;
}

int Bitmap::getHeight() const
{
	return m_height;
}

unsigned char* Bitmap::getData() const
{
	return m_data;
}

bool Bitmap::hasAlphaChannel() const
{
	return m_hasAlphaChannel;
}

void Bitmap::flipY()
{
	Bitmap temp(m_width, m_height, m_hasAlphaChannel);

	int channel = m_hasAlphaChannel ? 4 : 3;
	
	for (int x = 0; x < m_width; x++)
		for (int y = 0; y < m_height; y++)
			for (int i = 0; i < channel; i++)
				temp.m_data[(x + (m_height - y - 1) * m_width) * channel + i] = m_data[(x + y * m_width) * channel + i];
	
	for (int i = 0; i < m_width * m_height * channel; i++)
		m_data[i] = temp.m_data[i];
}

void Bitmap::flipX()
{
	Bitmap temp(m_width, m_height, m_hasAlphaChannel);

	int channel = m_hasAlphaChannel ? 4 : 3;
	for (int x = 0; x < m_width; x++)
		for (int y = 0; y < m_height; y++)
			for (int i = 0; i < channel; i++)
				temp.m_data[((m_width - x - 1) + y * m_width) * channel + i] = m_data[(x + y * m_width) * channel + i];
	
	for (int i = 0; i < m_width * m_height * channel; i++)
		m_data[i] = temp.m_data[i];
}