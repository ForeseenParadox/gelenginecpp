#pragma once
#include <SDL\SDL_image.h>
#include <string>

/* An array of pixel data that represents a 2D image. Contains many useful methods for manipulating this data. */
class Bitmap
{
private:
	/* A pointer to the start of the pixel data. The data is stored as unsigned bytes. */
	unsigned char* m_data;
	/* The width of the bitmap. */
	int m_width;
	/* The height of the bitmap. */
	int m_height;
	/* Flag determining storing if this bitmap has an alpha channel contained in its pixel data. */
	bool m_hasAlphaChannel;
public:
	/* Contructs a bitmap from the specified path. The pixel data will be loaded into RAM. */
	Bitmap(const std::string&);
	/* Creates a new blank bitmap that is the specified size */
	Bitmap(int w, int h, bool);
	/* Destroys this bitmap, all data for the current instance will be freed.*/
	~Bitmap();
	/* @return the width of the bitmap. */
	int getWidth() const;
	/* @return the height of the bitmap. */
	int getHeight() const;
	/* @return a pointer to the first element of data of this bitmap */
	unsigned char* getData() const;
	/* @return if this bitmap has an alpha channel contained in its pixel data. */
	bool hasAlphaChannel() const;

	/* Mutating manipulation methods */
	void flipX();
	void flipY();
};