#include "color.h"

Color::Color()
{
	m_r = m_g = m_b = m_a = 0;
}

Color::Color(float r, float g, float b)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = 1;
}

Color::Color(float r, float g, float b, float a)
{
	m_r = r;
	m_g = g;
	m_b = b;
	m_a = a;
}

Color::Color(int r, int g, int b)
{
	m_r = r / 255.0f;
	m_g = g / 255.0f;
	m_b = b / 255.0f;
	m_a = 1;
}

Color::Color(int r, int g, int b, int a)
{
	m_r = r / 255.0f;
	m_g = g / 255.0f;
	m_b = b / 255.0f;
	m_a = a / 255.0f;
}

float Color::getRed() const
{
	return m_r;
}

float Color::getGreen() const
{
	return m_g;
}

float Color::getBlue() const
{
	return m_b;
}

float Color::getAlpha() const
{
	return m_a;
}

int Color::getRed255() const
{
	return (int)(m_r * 255);
}

int Color::getGreen255() const
{
	return (int)(m_g * 255);
}

int Color::getBlue255() const
{
	return (int)(m_b * 255);
}

int Color::getAlpha255() const
{
	return (int)(m_a * 255);
}

void Color::setRed(float r)
{
	m_r = r;
}

void Color::setGreen(float g)
{
	m_g = g;
}

void Color::setBlue(float b)
{
	m_b = b;
}

void Color::setAlpha(float a)
{
	m_a = a;
}

void Color::setRed(int r)
{
	m_r = r / 255.0f;
}

void Color::setGreen(int g)
{
	m_g = g / 255.0f;
}

void Color::setBlue(int b)
{
	m_b = b / 255.0f;
}

void Color::setAlpha(int a)
{
	m_a = a / 255.0f;
}