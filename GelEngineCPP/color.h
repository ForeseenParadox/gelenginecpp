#pragma once

/* A color represented in RGBA format. Stores normalized components, which have a range of 0-1.*/
/* This class also contains functionality to take in unnormalized, ranged 0-255, data and convert it. */
class Color
{
private:
	/* The components of this color stored as normalized floats. */
	float m_r,m_g,m_b,m_a;
public:

	/* Constructs a black color. */
	Color();
	/* Constructs a color with the specified unnormalized RGB components. */
	Color(int, int, int);
	/* Constructs a color with the specified unnormalized RGB components. */
	Color(int, int, int, int);
	/* Constructs a color with the specified normalized RGB components. */
	Color(float, float, float);
	/* Constructs a color with the specified normalized RGB components. */
	Color(float, float, float, float);
	
	/* @return the normalized red value of this color */
	float getRed() const;
	/* @return the unnormalized red value of this color */
	int getRed255() const;
	/* @return the normalized green value of this color */
	float getGreen() const;
	/* @return the unnormalized green value of this color */
	int getGreen255() const;
	/* @return the normalized blue value of this color */
	float getBlue() const;
	/* @return the unnormalized blue value of this color */
	int getBlue255() const;
	/* @return the normalized alpha value of this color */
	float getAlpha() const;
	/* @return the unnormalized alpha value of this color */
	int getAlpha255() const;
	/* Sets the red component to the specified normalized value. */
	void setRed(float);
	/* Sets the red component to the specified unnormalized value. */
	void setRed(int);
	/* Sets the green component to the specified normalized value. */
	void setGreen(float);
	/* Sets the green component to the specified unnormalized value. */
	void setGreen(int);
	/* Sets the blue component to the specified normalized value. */
	void setBlue(float);
	/* Sets the blue component to the specified unnormalized value. */
	void setBlue(int);
	/* Sets the alpha component to the specified normalized value. */
	void setAlpha(float);
	/* Sets the alpha component to the specified unnormalized value. */
	void setAlpha(int);
};

/* The constant color red */
const Color RED(1, 0, 0, 1);
/* The constant color green */
const Color GREEN(1, 0, 0, 1);
/* The constant color blue */
const Color BLUE(1, 0, 0, 1);
/* The constant color magenta */
const Color MAGENTA(1, 0, 1, 1);
/* The constant color yellow */
const Color YELLOW(1, 1, 0, 1);
/* The constant color white */
const Color WHITE(1, 1, 1, 1);
/* The constant color black */
const Color BLACK(0, 0, 0, 1);
/* The constant color cyan */
const Color CYAN(0, 1, 1, 1);
