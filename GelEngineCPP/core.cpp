#include "core.h"
#include <SDL\SDL_image.h>

Core* Core::m_instance;

Core::Core(const CoreConfig& config)
{
	if (Core::m_instance != nullptr)
	{ 
		std::cerr << "Only one instance can be created of Core!";
	}
	else
	{
		Core::m_instance = this;
		setTargetFps(config.m_targetFps);
		setTargetUps(config.m_targetUps);
		m_game = config.m_game;
		m_running = false;

		// load sdl
		if (SDL_Init(SDL_INIT_EVERYTHING))
		{
			std::cerr << "Problem initializing SDL!" << std::endl;
		}

		int flags = IMG_INIT_PNG | IMG_INIT_JPG;
		if (IMG_Init(flags) != flags)
		{
			std::cerr << "Problem initializing SDL image!" << std::endl;
		}

		// load the window
		m_window = new Window(config.m_title, config.m_width, config.m_height, config.m_resizable, config.m_vsync);

		// load the input handler
		m_inputHandler = new InputHandler;
	}
}

Core::~Core()
{
	delete m_window;
	delete m_game;
	IMG_Quit();
	SDL_Quit();
}

void Core::start()
{
	m_running = true;
	run();
}

void Core::stop()
{
	m_running = false;
}

void Core::run()
{

	// init the game and engine
	init();

	int count = 0;
	float du = 0;
	float df = 0;
	int delta = 0;
	int lastFrame = SDL_GetTicks();
	int frames = 0;
	int updates = 0;

	while (isRunning())
	{
		int thisFrame = SDL_GetTicks();
		delta = thisFrame - lastFrame;
		lastFrame = thisFrame;

		du += delta;
		df += delta;
		count += delta;

		if (m_targetUps != -1)
		{
			while (du >= (1000.0f / getTargetUps()))
			{
				update();
				du -= (1000.0f / getTargetUps());
				updates++;
			}
		} else
		{
			update();
			du = 0;
			updates++;
		}

		if (m_targetFps != -1)
		{
			if (df >= (1000.0f / getTargetFps()))
			{
				render();
				df = 0;
				frames++;
			} else
			{
				// optimization so we are not in a busy loop
				// this will prevent the engine from being a hog
				SDL_Delay((int) ((1000.0f / getTargetFps()) - df));
			}
		} else
		{
			render();
			df = 0;
			frames++;
		}

		if (count >= 1000.0)
		{
			m_fps = frames;
			m_ups = updates;
			frames = 0;
			updates = 0;
			count = 0;
		}

	}
}

void Core::init()
{
	m_game->init();
}

void Core::resized()
{
	m_game->resized();
}

void Core::update()
{
	// poll events
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		if (event.type == SDL_QUIT)
			stop();
		else if (event.type == SDL_WINDOWEVENT_RESIZED)
			resized();
		else if (event.type == SDL_KEYDOWN || event.type == SDL_KEYUP)
			m_inputHandler->keyboardEvent(event.key);
		else if (event.type == SDL_MOUSEBUTTONDOWN || event.type == SDL_MOUSEBUTTONUP)
			m_inputHandler->mouseButtonEvent(event.button);
	}
	m_game->update();
}

void Core::render()
{
	glClearColor(0,0,0,1);
	glClear(GL_COLOR_BUFFER_BIT);

	m_game->render();

	m_window->update();
}

bool Core::isRunning() const
{
	return m_running;
}

int Core::getFps() const
{
	return m_fps;
}

int Core::getUps() const
{
	return m_ups;
}

int Core::getTargetFps() const
{
	return m_targetFps;
}

int Core::getTargetUps() const
{
	return m_targetUps;
}

Window* Core::getWindow() const
{
	return m_window;
}

BaseGame* Core::getBaseGame() const
{
	return m_game;
}

InputHandler* Core::getInput() const
{
	return m_inputHandler;
}

void Core::setTargetFps(int val)
{
	m_targetFps = val;
}

void Core::setTargetUps(int val)
{
	m_targetUps = val;
}

Core* Core::getInstance()
{
	return m_instance;
}