#pragma once
#include "window.h"
#include "baseGame.h"
#include <string>
#include <GL\glew.h>
#include "input.h"

/* A struct used to set the initial configuration of the core engine. */
struct CoreConfig
{
	/* A pointer to the game that will respond to the engine event loop. */
	BaseGame* m_game;
	/* The title of the window. */
	std::string m_title;
	/* The width of the window. */
	int m_width;
	/* The height of the window. */
	int m_height;
	/* A flag for a window's resizability. */
	bool m_resizable;
	/* A flag for window vsync status. */
	bool m_vsync;
	/* The FPS cap for the engine and game. */
	int m_targetFps;
	/* The UPS cap for the engine and game. */
	int m_targetUps;
};

/* The main core mechanic class of the engine, used to contain and execute the main engine event loop.*/
class Core
{
private:
	/* Specifies if the main event loop is executing. */
	bool m_running;
	/* The current frames per second. */
	int m_fps;
	/* The current updates per second. */
	int m_ups;
	/* The current target fps. */
	int m_targetFps;
	/* The current target ups. */
	int m_targetUps;
	/* A pointer to a window. */
	Window* m_window;
	/* A pointer to the polymorphic base game.*/
	BaseGame* m_game;
	/* A pointer to the input handler of the engine. */
	InputHandler* m_inputHandler;
	/* A singleton of this class. */
	static Core* m_instance;
	/* Starts execution of the main engine event loop. */
	void run();
	/* Initializes the engine and game program. */
	void init();
	/* Invoked when the window is resized. */
	void resized();
	/* Updates the engine and game. */
	void update();
	/* Renders the engine and game. */
	void render();
public:
	/* Creates a new core with the specified base game pointer and window properties. */
	Core(const CoreConfig&);
	/* Destructs this core and frees all game and engine resources. */
	~Core();
	/* Starts the execution of the engine event loop. */
	void start();
	/* Stops the execution of the engine event loop. */
	void stop();
	/* @return a flag representing if the engine is executing the event loop. */
	bool isRunning() const;
	/* @return the current frames per second. */
	int getFps() const;
	/* @return the current updates per second. */
	int getUps() const;
	/* @return the current target fps. */
	int getTargetFps() const;
	/* @return the current target ups. */
	int getTargetUps() const;
	/* @return a pointer to the window of the engine. */
	Window* getWindow() const;
	/* @return a pointer to the polymorphic base game. */
	BaseGame* getBaseGame() const;
	/* @return a pointer to the input handler of the engine. */
	InputHandler* getInput() const;
	/* Sets the target fps to the specified value. The engine event loop will try its best to limit its frames per second to this amount.*/
	void setTargetFps(int);
	/* Sets the target ups to the specified value. The engine event loop will try its best to limit its updates per second to this amount.*/
	void setTargetUps(int);

	static Core* getInstance();
};