#include "input.h"
#include <iostream>

InputHandler::InputHandler(){}
InputHandler::~InputHandler(){}

bool InputHandler::isKeyDown(int code)
{
	return keysDown[code];
}

bool InputHandler::isButtonDown(int code)
{
	return buttonsDown[code];
}

void InputHandler::keyboardEvent(SDL_KeyboardEvent& keyEvent)
{
	if (keyEvent.type == SDL_KEYDOWN)
		keysDown[keyEvent.keysym.sym] = true;
	else if (keyEvent.type == SDL_KEYUP)
		keysDown[keyEvent.keysym.sym] = false;
}

void InputHandler::mouseButtonEvent(SDL_MouseButtonEvent& mouseButtonEvent)
{
	if (mouseButtonEvent.type == SDL_MOUSEBUTTONDOWN)
		buttonsDown[mouseButtonEvent.button] = true;
	if (mouseButtonEvent.type == SDL_MOUSEBUTTONUP)
		buttonsDown[mouseButtonEvent.button] = false;

	std::cout << mouseButtonEvent.button;
}