#pragma once

#include <SDL\SDL.h>

const int KEYS = 256;
const int BUTTONS = 3;

class InputHandler
{
	friend class Core;
private:
	bool keysDown[KEYS];
	bool buttonsDown[BUTTONS];
	void keyboardEvent(SDL_KeyboardEvent& keyboardEvent);
	void mouseButtonEvent(SDL_MouseButtonEvent& mouseButtonEvent);
public:
	InputHandler();
	~InputHandler();

	bool isKeyDown(int code);
	bool isButtonDown(int code);
};