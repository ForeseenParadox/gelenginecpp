#include "ioutils.h"
#include <fstream>
#include <iostream>
#include <Windows.h>

std::string* loadFileAsString(const std::string& loc)
{
	std::ifstream istream;
	istream.open(loc.c_str());

	if (istream.is_open())
	{
		std::string* file = new std::string;
		
		while (istream.good())
		{
			std::string line;
			std::getline(istream, line);
			(*file) += line + "\n";
		}

		return file;
	} else
	{
		std::cerr << "Could not open file at: " << loc << std::endl;
		return nullptr;
	}
}