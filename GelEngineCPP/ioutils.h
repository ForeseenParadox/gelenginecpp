#pragma once
#include <string>
#include <vector>

// return a string instead of a pointer

/* Loads the file of the specified path as a string and returns a pointer to the string. */
std::string* loadFileAsString(const std::string&);