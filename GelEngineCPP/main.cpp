#pragma once

#include <iostream>
#include <GL\glew.h>
#include <string>
#include "baseGame.h"
#include "core.h"
#include "ioutils.h"
#include <vector>
#include "vao.h"
#include "vbo.h"
#include "shader.h"
#include "bitmap.h"
#include "texture.h"
#include "spriteBatch.h"

const std::string TITLE = "Game";
const int WIDTH = 16*70;
const int HEIGHT = 9*70;

// COLOR CTORS NEED TO BE FIXED

class Game : public BaseGame
{
private:
	//GLuint vao, vbo;
	float rot, scale=1, x;
	Texture portal;
	TextureRegion* region;
	SpriteBatch* batch;
public:
	~Game()
	{
		delete batch;
	}
	void init()
	{
		portal.load("./BlockPieces.png");
		region = new TextureRegion(portal, 24, 24 * 3, 24, 24);
		batch = new SpriteBatch;
	}

	void resized()
	{

	}
	
	void update()
	{
		Core::getInstance()->getWindow()->setTitle(std::to_string(Core::getInstance()->getFps()));

		rot += 0.01f;
	} 
	
	void render()
	{
		batch->setTint(Color(0.0f, 1.0f, 0.0f, 1.0f));

		batch->begin();
		batch->render(*region, 12,12,200,200, rot,4,4);
		batch->end();
	}
};


int main(int argc, char** argv)
{
	CoreConfig config;
	config.m_game = new Game;
	config.m_height = HEIGHT;
	config.m_width = WIDTH;
	config.m_title = TITLE;
	config.m_targetFps = -1;
	config.m_targetUps = 60;
	config.m_vsync = false;

	Core core(config);
	core.start();
	
	return 0;
}