#include "math.h"

Mat4f getTranslationMatrix(float x, float y)
{
	Mat4f res;
	asTranslationMatrix(res, x, y);
	return res;
}

Mat4f getRotationMatrix(float zRot)
{
	Mat4f res;
	asRotationMatrix(res, zRot);
	return res;
}

Mat4f getScaleMatrix(float scaleX, float scaleY)
{
	Mat4f res;
	asScaleMatrix(res,scaleX,scaleY);
	return res;
}

Mat4f getOrthoMatrix(float left, float right, float bottom, float top, float zNear, float zFar)
{
	Mat4f res;
	asOrthoMatrix(res, left, right, bottom, top, zNear, zFar);
	return res;
}

void asTranslationMatrix(Mat4f& mat, float x, float y)
{
	mat.loadIdentity();
	mat.setElement(0, 3, x);
	mat.setElement(1, 3, y);
}

void asRotationMatrix(Mat4f& mat, float zRot)
{
	mat.loadIdentity();
	mat.setElement(0, 0, (float)cos(zRot));
	mat.setElement(0, 1, (float)-sin(zRot));
	mat.setElement(1, 0, (float)sin(zRot));
	mat.setElement(1, 1, (float)cos(zRot));
}

void asScaleMatrix(Mat4f& mat, float scaleX, float scaleY)
{
	mat.loadIdentity();
	mat.setElement(0, 0, scaleX);
	mat.setElement(1, 1, scaleY);
}

void asOrthoMatrix(Mat4f& mat, float left, float right, float bottom, float top, float zNear, float zFar)
{
	mat.loadIdentity();
	mat.setElement(0, 0, 2 / (right - left));
	mat.setElement(1, 1, 2 / (top - bottom));
	mat.setElement(2, 2, -2 / (zFar - zNear));
	mat.setElement(0, 3, -(right+left)/(right - left));
	mat.setElement(1, 3, -(top+bottom)/(top - bottom));
	mat.setElement(2, 3, (zFar+zNear) / (zFar - zNear));
}


Vec2f::Vec2f()
{
	m_x = 0;
	m_y = 0;
}

Vec2f::Vec2f(const Vec2f& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
}

Vec2f::Vec2f(float xv, float yv)
{
	m_x = xv;
	m_y = yv;
}

float Vec2f::getX() const
{
	return m_x;
}

float Vec2f::getY() const
{
	return m_y;
}

float Vec2f::length() const
{
	return sqrt(m_x*m_x + m_y*m_y);
}

float Vec2f::dot(const Vec2f& other) const
{
	return m_x * other.m_x + m_y * other.m_y;
}

Vec2f Vec2f::operator+(const Vec2f& other) const
{
	return Vec2f(m_x + other.m_x, m_y + other.m_y);
}

Vec2f Vec2f::operator-(const Vec2f& other) const
{
	return Vec2f(m_x - other.m_x, m_y - other.m_y);
}

bool Vec2f::operator==(const Vec2f& other) const
{
	return m_x == other.m_x && m_y == other.m_y;
}

void Vec2f::setX(float xv)
{
	m_x = xv;
}

void Vec2f::setY(float yv)
{
	m_y = yv;
}

void Vec2f::scale(float scaler)
{
	m_x *= scaler;
	m_y *= scaler;
}

void Vec2f::rotate(float theta)
{
	m_x = m_x * cos(theta) - m_y * sin(theta);
	m_y = m_x * sin(theta) + m_y * cos(theta);
}

// Vec3f
Vec3f::Vec3f()
{
	m_x = 0;
	m_y = 0;
	m_z = 0;
}

Vec3f::Vec3f(const Vec3f& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
	m_z = other.m_z;
}

Vec3f::Vec3f(float xv, float yv, float zv)
{
	m_x = xv;
	m_y = yv;
	m_z = zv;
}

Vec3f::Vec3f(const Vec2f& vec, float zv)
{
	m_x = vec.m_x;
	m_y = vec.m_y;
	m_z = zv;
}

Vec3f::Vec3f(float xv, const Vec2f& vec)
{
	m_x = xv;
	m_y = vec.m_x;
	m_z = vec.m_y;
}

float Vec3f::getX() const
{
	return m_x;
}

float Vec3f::getY() const
{
	return m_y;
}

float Vec3f::getZ() const
{
	return m_z;
}

float Vec3f::length() const
{
	return sqrt(m_x*m_x + m_y*m_y + m_z*m_z);
}

float Vec3f::dot(const Vec3f& other) const
{
	return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z;
}

Vec3f Vec3f::operator+(const Vec3f& other) const
{
	return Vec3f(m_x + other.m_x, m_y + other.m_y, m_z + other.m_z);
}

Vec3f Vec3f::operator-(const Vec3f& other) const
{
	return Vec3f(m_x - other.m_x, m_y - other.m_y, m_z - other.m_z);
}

bool Vec3f::operator==(const Vec3f& other) const
{
	return m_x == other.m_x && m_y == other.m_y && m_z == other.m_z;
}

void Vec3f::setX(float xv)
{
	m_x = xv;
}

void Vec3f::setY(float yv)
{
	m_y = yv;
}

void Vec3f::setZ(float zv)
{
	m_z = zv;
}

void Vec3f::scale(float scaler)
{
	m_x *= scaler;
	m_y *= scaler;
	m_z *= scaler;
}

// Vec4f
Vec4f::Vec4f()
{
	m_x = 0;
	m_y = 0;
	m_z = 0;
	m_w = 0;
}

Vec4f::Vec4f(const Vec4f& other)
{
	m_x = other.m_x;
	m_y = other.m_y;
	m_z = other.m_z;
	m_w = other.m_w;
}

Vec4f::Vec4f(float xv, float yv, float zv, float wv)
{
	m_x = xv;
	m_y = yv;
	m_z = zv;
	m_w = wv;
}

Vec4f::Vec4f(const Vec3f& vec, float wv)
{
	m_x = vec.m_x;
	m_y = vec.m_y;
	m_z = vec.m_z;
	m_w = wv;
}

Vec4f::Vec4f(float xv, const Vec3f& vec)
{
	m_x = xv;
	m_y = vec.m_x;
	m_z = vec.m_y;
	m_w = vec.m_z;
}

float Vec4f::getX() const
{
	return m_x;
}

float Vec4f::getY() const
{
	return m_y;
}

float Vec4f::getZ() const
{
	return m_z;
}

float Vec4f::getW() const
{
	return m_w;
}

float Vec4f::length() const
{
	return sqrt(m_x*m_x + m_y*m_y + m_z*m_z + m_w*m_w);
}

float Vec4f::dot(const Vec4f& other) const
{
	return m_x * other.m_x + m_y * other.m_y + m_z * other.m_z + m_w*other.m_w;
}

Vec4f Vec4f::operator+(const Vec4f& other) const
{
	return Vec4f(m_x + other.m_x, m_y + other.m_y, m_z + other.m_z, m_w + other.m_w);
}

Vec4f Vec4f::operator-(const Vec4f& other) const
{
	return Vec4f(m_x - other.m_x, m_y - other.m_y, m_z - other.m_z, m_w - other.m_w);
}

bool Vec4f::operator==(const Vec4f& other) const
{
	return m_x == other.m_x && m_y == other.m_y && m_z == other.m_z && m_w == other.m_w;
}

void Vec4f::setX(float xv)
{
	m_x = xv;
}

void Vec4f::setY(float yv)
{
	m_y = yv;
}

void Vec4f::setZ(float zv)
{
	m_z = zv;
}

void Vec4f::setW(float wv)
{
	m_w = wv;
}

void Vec4f::scale(float scaler)
{
	m_x *= scaler;
	m_y *= scaler;
	m_z *= scaler;
	m_w *= scaler;
}

Mat4f::Mat4f()
{
	loadZero();
}

Mat4f::Mat4f(float values[4][4])
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			setElement(i, j, values[i][j]);
		}
	}
}

Mat4f::Mat4f(Vec4f* values)
{
	for (int j = 0; j < 4; j++)
	{
		setElement(0,j, values[0].getX());
		setElement(1,j, values[0].getY());
		setElement(2,j, values[0].getZ());
		setElement(3,j, values[0].getW());
	}
}

bool Mat4f::inBounds(int i, int j) const
{
	return i >= 0 && j >= 0 && i < 4 && j < 4;
}

float Mat4f::getElement(int i, int j) const
{
	if (inBounds(i, j))
	{
		return m_mat[i][j];
	} else
	{
		return NAN;
	}
}

void Mat4f::setElement(int i, int j, float v)
{
	if (inBounds(i, j))
	{
		m_mat[i][j] = v;
	}
}

Mat4f Mat4f::operator+(const Mat4f& val) const
{
	Mat4f res;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			res.setElement(i, j, getElement(i, j) + val.getElement(i, j));
		}
	}
	return res;
}

Mat4f Mat4f::operator-(const Mat4f& val) const
{
	Mat4f res;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			res.setElement(i, j, getElement(i, j) - val.getElement(i, j));
		}
	} 
	return res;
}

Mat4f Mat4f::operator*(const Mat4f& val) const
{
	Mat4f res;
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			Vec4f rowVec(getElement(i, 0), getElement(i, 1), getElement(i, 2), getElement(i, 3));
			Vec4f colVec(val.getElement(0, j), val.getElement(1, j), val.getElement(2, j), val.getElement(3, j));
			res.setElement(i, j, rowVec.dot(colVec));
		}
	}
	return res;
}

Vec4f Mat4f::operator*(const Vec4f& val) const
{
	Vec4f res;
	for (int i = 0; i < 4; i++)
	{
		Vec4f rowVec(getElement(i, 0), getElement(i, 1), getElement(i, 2), getElement(i, 3));
		float dot = rowVec.dot(val);
		if (i == 0)
			res.setX(dot);
		else if (i == 1)
			res.setY(dot);
		else if (i == 2)
			res.setZ(dot);
		else if (i == 3)
			res.setW(dot);
	}
	return res;
}

void Mat4f::scale(float scaler)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			setElement(i, j, getElement(i, j)*scaler);
		}
	}
}

void Mat4f::loadIdentity()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			if (i == j)
			{
				setElement(i, j, 1);
			} else
			{
				setElement(i, j, 0);
			}
		}
	}
}

void Mat4f::loadZero()
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
		{
			setElement(i, j, 0);
		}
	}
}
