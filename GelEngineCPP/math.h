#pragma once
#include <cmath>

class Vec2f;
class Vec3f;
class Vec4f;
class Mat4f;

/* @return a translation matrix with the specified translation */
Mat4f getTranslationMatrix(float, float);
/* @return a rotation matrix about the z axis with the specified rotation */
Mat4f getRotationMatrix(float);
/* @return a scale matrix with the specified scale */
Mat4f getScaleMatrix(float, float);
/* @return a orthographic projection matrix with the specified clipping planes */
Mat4f getOrthoMatrix(float, float, float, float, float, float);
/* Changes the state of the specified matrix to a translation matrix with the specified translation. */
void asTranslationMatrix(Mat4f&, float, float);
/* Changes the state of the specified matrix to a rotation matrix with the specified rotation about the z axis. */
void asRotationMatrix(Mat4f&, float);
/* Changes the state of the specified matrix to a scale matrix with the specified scale. */
void asScaleMatrix(Mat4f&, float, float);
/* Changes the state of the specified matrix to a orthographic projection matrix with the specified clipping planes. */
void asOrthoMatrix(Mat4f&, float, float, float, float, float, float);

/* Represents a real world vector with 2 elements. */
class Vec2f
{
	friend class Vec3f;
	friend class Vec4f;
private:
	/* The x and y components of this vector. */
	float m_x, m_y;
public:
	/* Creates a zero vector. */
	Vec2f();
	/* Copies the specified vector */
	Vec2f(const Vec2f&);
	/* Creates a vector with the specified components */
	Vec2f(float, float);
	/* @return the x component of this vector */
	float getX() const;
	/* @ return the y component of this vector */
	float getY() const;
	/* @return the magnitude of this vector */
	float length() const;
	/* @return the dot product between this vector and the specified vector */
	float dot(const Vec2f&) const;
	/* @return the result of adding this vector with the specified vector. */
	Vec2f operator+(const Vec2f&) const;
	/* @return the result of subtracting this vector with the specified vector. */
	Vec2f operator-(const Vec2f&) const;
	/* Checks if this vector is equal to the specified vector. */
	bool operator==(const Vec2f&) const;
	/* Sets the x component of this vector. */
	void setX(float);
	/* Sets the y component of this vector. */
	void setY(float);
	/* Scales this vector by the specified amount, this will mutate this vector's data. */
	void scale(float);
	/* Rotates this vector by the specified amount. */
	void rotate(float);
};

/* Represents a real world vector with 3 elements. */
class Vec3f
{
	friend class Vec2f;
	friend class Vec4f;
private:
	/* The components of this vector. */
	float m_x, m_y, m_z;
public:
	/* Creates a zero vector. */
	Vec3f();
	/* Creates a copy of the specified vector. */
	Vec3f(const Vec3f&);
	/* Creates a vector with the specified components */
	Vec3f(float, float, float);
	/* Creates a vector with the specified components */
	Vec3f(const Vec2f&, float);
	/* Creates a vector with the specified components */
	Vec3f(float, const Vec2f&);
	/* @ return the x component of this vector */
	float getX() const;
	/* @ return the y component of this vector */
	float getY() const;
	/* @ return the z component of this vector */
	float getZ() const;
	/* @return the magnitude of this vector */
	float length() const;
	/* @return the dot product between this vector and the specified vector */
	float dot(const Vec3f&) const;
	Vec3f cross(const Vec3f&) const;
	/* @return the result of adding this vector with the specified vector. */
	Vec3f operator+(const Vec3f&) const;
	/* @return the result of subtracting this vector with the specified vector. */
	Vec3f operator-(const Vec3f&) const;
	/* Checks if this vector is equal to the specified vector. */
	bool operator==(const Vec3f&) const;
	/* Sets the x component of this vector. */
	void setX(float);
	/* Sets the y component of this vector. */
	void setY(float);
	/* Sets the z component of this vector. */
	void setZ(float);
	/* Scales this vector by the specified amount, this will mutate this vector's data. */
	void scale(float);
};

/* Represents a real world vector with 4 elements. */
class Vec4f
{
	friend class Vec2f;
	friend class Vec3f;
private:
	/* The components of this vector. */
	float m_x, m_y, m_z, m_w;
public:
	/* Creates a zero vector. */
	Vec4f();
	/* Creates a copy of the specified vector. */
	Vec4f(const Vec4f&);
	/* Creates a vector with the specified components */
	Vec4f(float, float, float, float);
	/* Creates a vector with the specified components */
	Vec4f(const Vec3f&, float);
	/* Creates a vector with the specified components */
	Vec4f(float, const Vec3f&);
	/* @ return the x component of this vector */
	float getX() const;
	/* @ return the y component of this vector */
	float getY() const;
	/* @ return the z component of this vector */
	float getZ() const;
	/* @ return the w component of this vector */
	float getW() const;
	/* @return the magnitude of this vector */
	float length() const;
	/* @return the dot product between this vector and the specified vector */
	float dot(const Vec4f&) const;
	/* @return the result of adding this vector with the specified vector. */
	Vec4f operator+(const Vec4f&) const;
	/* @return the result of subtracting this vector with the specified vector. */
	Vec4f operator-(const Vec4f&) const;
	/* Checks if this vector is equal to the specified vector. */
	bool operator==(const Vec4f&) const;
	/* Sets the x component of this vector. */
	void setX(float);
	/* Sets the y component of this vector. */
	void setY(float);
	/* Sets the z component of this vector. */
	void setZ(float);
	/* Sets the w component of this vector. */
	void setW(float);
	/* Scales this vector by the specified amount, this will mutate this vector's data. */
	void scale(float);
};

/* Represents a real world 4 by 4 matrix. */
class Mat4f
{
private:
	/* The elements of this matrix. */
	float m_mat[4][4];
	/* @return if the specified position is in the bounds of this matrix. */
	bool inBounds(int, int) const;
public:
	/* Creates a zero matrix. */
	Mat4f();
	/* Creates a matrix with the specified values. */
	Mat4f(float values[4][4]);
	/* Creates a matrix with an array of 4 vec4f's representing the rows of this matrix. */
	Mat4f(Vec4f*);
	/* @return the element at the specified position, which must be in bounds. */
	float getElement(int, int) const;
	/* Set the element at the specified position to the specified value. The position must be in bounds. */
	void setElement(int, int, float);
	/* Returns the result of the sum of this matrix and the specified matrix. */
	Mat4f operator+(const Mat4f&) const;
	/* Returns the result of the difference of this matrix and the specified matrix. */
	Mat4f operator-(const Mat4f&) const;
	/* Returns the result of the product of this matrix and the specified matrix. */
	Mat4f operator*(const Mat4f&) const;
	/* Returns the result of the product of this matrix and the specified vector. */
	Vec4f operator*(const Vec4f&) const;
	/* Scales this matrix by the specified amount. This will mutate this matrix. */
	void scale(float scaler);
	/* Mutates this matrices data into the identity matrix. */
	void loadIdentity();
	/* Mutates this matrices data into the zero matrix. */
	void loadZero();
};