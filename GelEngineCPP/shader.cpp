#include "shader.h"
#include <iostream>

ShaderProgram::ShaderProgram(const std::string& vsPath, const std::string& fsPath)
{
	m_programHandle = glCreateProgram();
	createAndCompileVertexShader(vsPath);
	createAndCompileFragmentShader(fsPath);

	// you must bind attributes before you link the shader program!
	bindVertexAttrib(0, "position");
	bindVertexAttrib(1, "color");
	bindVertexAttrib(2, "texCoord");

	linkProgram();
}

int ShaderProgram::getProgramHandle() const
{
	return m_programHandle;
}

int ShaderProgram::getVSHandle() const
{
	return m_vsHandle;
}

int ShaderProgram::getFSHandle() const
{
	return m_fsHandle;
}

void ShaderProgram::bind() const
{
	glUseProgram(m_programHandle);
}

void ShaderProgram::createAndCompileVertexShader(const std::string& path)
{
	m_vsHandle = createAndCompileShader(GL_VERTEX_SHADER, *loadFileAsString(path));
	
	if (getShaderCompileStatus(m_vsHandle) == GL_FALSE)
	{
		std::cerr << "Failed to compile vertex shader at: " << path << "\n" << *getShaderInfoLog(m_vsHandle);
	}
}

void ShaderProgram::createAndCompileFragmentShader(const std::string& path)
{
	m_fsHandle = createAndCompileShader(GL_FRAGMENT_SHADER, *loadFileAsString(path));

	if (getShaderCompileStatus(m_fsHandle) == GL_FALSE)
	{
		std::cerr << "Failed to compile fragment shader at: " << path << "\n" << *getShaderInfoLog(m_fsHandle);
	}
}

int ShaderProgram::createAndCompileShader(GLenum type, const std::string& source)
{
	int handle = glCreateShader(type);
	const GLchar* ptr = source.c_str();
	glShaderSource(handle,1,&ptr,NULL);
	glCompileShader(handle);
	return handle;
}

int ShaderProgram::getShaderInfoLogLength(int shader) const
{
	int l = 0;
	glGetShaderiv(shader, GL_INFO_LOG_LENGTH, &l);
	return l;
}

std::string* ShaderProgram::getShaderInfoLog(int shader) const
{
	char* log = new char;
	glGetShaderInfoLog(shader, getShaderInfoLogLength(shader), NULL, log);
	return new std::string(log);
}

int ShaderProgram::getLinkStatus() const
{
	int status = 0;
	glGetProgramiv(m_programHandle, GL_LINK_STATUS, &status);
	return status;
}

int ShaderProgram::getShaderCompileStatus(int shader) const
{
	int status = 0;
	glGetShaderiv(shader, GL_COMPILE_STATUS, &status);
	return status;
}

void ShaderProgram::linkProgram()
{
	glAttachShader(m_programHandle, m_vsHandle);
	glAttachShader(m_programHandle, m_fsHandle);
	glLinkProgram(m_programHandle);

	if (getLinkStatus() == GL_FALSE)
	{
		std::string* err = getProgramInfoLog();
		std::cerr << "Failed to link shader: \n" << *getProgramInfoLog() << std::endl;
	}
}

int ShaderProgram::getProgramInfoLogLength() const
{
	int l = 0;
	glGetProgramivARB(m_programHandle, GL_INFO_LOG_LENGTH, &l);
	return l;
}

std::string* ShaderProgram::getProgramInfoLog() const
{
	GLchar* log = nullptr;
	glGetProgramInfoLog(m_programHandle, getProgramInfoLogLength(), NULL, log);
	return new std::string(log);
}

void ShaderProgram::bindVertexAttrib(int loc, const std::string& name)
{
	glBindAttribLocation(m_programHandle, loc, name.c_str());
}

int ShaderProgram::getUniformLocation(const std::string& name) const
{
	return glGetUniformLocation(m_programHandle, name.c_str());
}

void ShaderProgram::setUniformFloat(const std::string& name, float f)
{
	bind();
	glUniform1f(getUniformLocation(name), f);
}

void ShaderProgram::setUniformInt(const std::string& name, int i)
{
	bind();
	glUniform1i(getUniformLocation(name), i);
}

void ShaderProgram::setUniformVec2f(const std::string& name, const Vec2f& vec)
{
	bind();
	glUniform2f(getUniformLocation(name), vec.getX(), vec.getY());
}

void ShaderProgram::setUniformVec3f(const std::string& name, const Vec3f& vec)
{
	bind();
	glUniform3f(getUniformLocation(name), vec.getX(), vec.getY(), vec.getZ());
}

void ShaderProgram::setUniformVec4f(const std::string& name, const Vec4f& vec)
{
	bind();
	glUniform4f(getUniformLocation(name), vec.getX(), vec.getY(), vec.getZ(), vec.getW());
}

void ShaderProgram::setUniformMat4f(const std::string& name, const Mat4f& mat)
{
	bind();
	float* newMat = new float[16];
	for (int j = 0; j < 4; j++)
	{
		for (int i = 0; i < 4; i++)
		{
			newMat[j * 4 + i] = mat.getElement(i, j);
		}
	}
	glUniformMatrix4fv(getUniformLocation(name), 1, GL_FALSE, newMat);
}