#pragma once
#include <string>
#include <GL\glew.h>
#include "ioutils.h"
#include "math.h"

/* A GLSL shader program that can be loaded to use in the OpenGL pipeline. In the context of this engine, A shader 
program consists of a vertex shader and fragment shader that can be loaded, compiled and linked to be used on the GPU. */
class ShaderProgram
{
private:
	/* The OpenGL handle of this program. */
	int m_programHandle;
	/* The OpenGL handle of the vertex shader. */
	int m_vsHandle;
	/* The OpenGL handle of the fragment shader. */
	int m_fsHandle;
	/* Gets the info log of the specified shader program. This will return warnings and diagnostic info, if any for that shader. 
	 @return the info log of the specified shader */
	std::string* getShaderInfoLog(int) const;
	/* @return the length of the info log of the specified shader */
	int getShaderInfoLogLength(int) const;
	/* @return the compile status of the specified shader. */
	int getShaderCompileStatus(int) const;
	/* @return the link status of the specified shader. */
	int getLinkStatus() const;
	/* @return the info log length of the program. */
	int getProgramInfoLogLength() const;
	/* Gets the info log of the current shader program.This will return warnings and diagnostic info, if any for this program.
	@return the info log of the specified shader. NOTE: This is not the same as getShaderInfoLog(). That function is specific for a shader, this is for the program as a whole. */ 
	std::string* getProgramInfoLog() const;
	/* @return the unform location of the specified uniform name*/
	int getUniformLocation(const std::string&) const;
public:
	/* Constructs a shader with the path to the vertex shader and fragment shader, respectively. */
	ShaderProgram(const std::string&, const std::string&);
	/* @return the OpenGL program handle for this shader program */
	int getProgramHandle() const;
	/* @return the OpenGL vertex shader handle for this shader program*/
	int getVSHandle() const;
	/* @return the OpenGL fragment shader handle for this shader program*/
	int getFSHandle() const;
	/* Binds this shader for use in the OpenGL pipeline. */
	void bind() const;
	/* Creates and compiles the specified type of shader with the specified path to the source. */
	int createAndCompileShader(GLenum type, const std::string&);
	/* Creates and compiles the vertex shader for this program referenced by the specified path. */
	void createAndCompileVertexShader(const std::string&);
	/* Creates and compiles the fragment shader for this program referenced by the specified path. */
	void createAndCompileFragmentShader(const std::string&);
	/* Links this GLSL shader program. This process is the final stage in shader compilation and should be called after shader creation and compilation. */
	void linkProgram(); 
	/* Binds the specified vertex attrib location to the specified name. */
	void bindVertexAttrib(int, const std::string&);
	/* Sets the float uniform with the specified name to the specified value. */
	void setUniformFloat(const std::string&, float);
	/* Sets the int uniform with the specified name to the specified value. */
	void setUniformInt(const std::string&, int);
	/* Sets the vec2f uniform with the specified name to the specified value. */
	void setUniformVec2f(const std::string&, const Vec2f&);
	/* Sets the vec3f uniform with the specified name to the specified value. */
	void setUniformVec3f(const std::string&, const Vec3f&);
	/* Sets the vec4f uniform with the specified name to the specified value. */
	void setUniformVec4f(const std::string&, const Vec4f&);
	/* Sets the mat4f uniform with the specified name to the specified value. */
	void setUniformMat4f(const std::string&, const Mat4f&);
};