#include "spriteBatch.h"
#include "core.h"

SpriteBatch::SpriteBatch() :
m_shader("./basicShader.vs", "./basicShader.fs"),
m_positionBuffer(50000 * 2),
m_colorBuffer(50000 * 4),
m_textureCoordBuffer(50000 * 2),
m_tint(1.0f, 1.0f, 1.0f, 1.0f)
{
	init(50000);
}

SpriteBatch::SpriteBatch(int maxVertices) :
m_shader("./basicShader.vs", "./basicShader.fs"),
m_positionBuffer(maxVertices * 2),
m_colorBuffer(maxVertices * 4),
m_textureCoordBuffer(maxVertices * 2),
m_tint(1.0f, 1.0f, 1.0f, 1.0f)
{
	init(maxVertices);
}

SpriteBatch::~SpriteBatch()
{
	m_texture = nullptr;
}

void SpriteBatch::init(int maxVertices)
{
	m_maxVertices = maxVertices;
	m_texture = nullptr;
	asOrthoMatrix(m_viewProjectionMatrix, 0.0f, static_cast<float>(Core::getInstance()->getWindow()->getWidth()), 0.0f, static_cast<float>(Core::getInstance()->getWindow()->getHeight()), -0.01f, 1000.0f);
	began = false;
}

void SpriteBatch::flush()
{
	if (began)
	{
		m_colorBuffer.sendData();
		m_positionBuffer.sendData();
		m_textureCoordBuffer.sendData();

		m_vao.update(m_positionBuffer, 0, 2);
		m_vao.update(m_colorBuffer, 1, 4);
		m_vao.update(m_textureCoordBuffer, 2, 2);

		m_positionBuffer.clearBuffer();
		m_colorBuffer.clearBuffer();
		m_textureCoordBuffer.clearBuffer();

		m_shader.bind();
		m_texture->bind();
		asOrthoMatrix(m_viewProjectionMatrix, 0.0f, static_cast<float>(Core::getInstance()->getWindow()->getWidth()), 0.0f, static_cast<float>(Core::getInstance()->getWindow()->getHeight()), -0.01f, 1000.0f);
		m_shader.setUniformMat4f("viewProjectionMatrix", m_viewProjectionMatrix);

		m_vao.render(GL_QUADS, m_vertices);
		m_vertices = 0;
	}
}

void SpriteBatch::begin()
{
	if (began)
	{
		std::cerr << "Can not begin sprite rendering mode whiles already in sprite rendering mode";
	} else
	{
		began = true;
		m_vertices = 0;

		m_positionBuffer.clearBuffer();
		m_colorBuffer.clearBuffer();
		m_textureCoordBuffer.clearBuffer();
	}
}

void SpriteBatch::end()
{
	if (!began)
	{
		std::cerr << "Must have already began to end drawing mode in sprite batch";
	} else
	{
		flush();	
		began = false;
	}
}

void SpriteBatch::render(const TextureRegion& texture, float x, float y)
{
	render(texture, x, y, 0);
}

void SpriteBatch::render(const TextureRegion& texture, float x, float y, float rot)
{
	render(texture, texture.getUnnormalizedWidth() / 2, texture.getUnnormalizedHeight() / 2, x, y, rot);
}

void SpriteBatch::render(const TextureRegion& texture, float ox, float oy, float x, float y, float rot)
{
	render(texture, ox, oy, x, y, rot, 1, 1);
}

void SpriteBatch::render(const TextureRegion& texture, float ox, float oy, float x, float y, float rot, float sx, float sy)
{
	if (!began)
		return;
	if (m_vertices >= m_maxVertices)
		flush();
	if (m_texture == nullptr)
		m_texture = (Texture*) (&texture.m_texture);
	if (texture.m_texture.getHandle() != m_texture->getHandle())
	{
		flush();
		m_texture = (Texture*)(&texture.m_texture);
	}

	float w = texture.getUnnormalizedWidth();
	float h = texture.getUnnormalizedHeight();
	
	Vec4f originTranslate(ox, oy, 0, 0);
	Vec4f v1(0, 0, 0, 0), v2(0, h, 0, 0), v3(w, h, 0, 0), v4(w, 0, 0, 0);
	v1 = v1 - originTranslate;
	v2 = v2 - originTranslate;
	v3 = v3 - originTranslate;
	v4 = v4 - originTranslate;

	v1.setX(v1.getX() * sx);
	v1.setY(v1.getY() * sy);
	v2.setX(v2.getX() * sx);
	v2.setY(v2.getY() * sy);
	v3.setX(v3.getX() * sx);
	v3.setY(v3.getY() * sy);
	v4.setX(v4.getX() * sx);
	v4.setY(v4.getY() * sy);

	if (rot != 0)
	{
		Mat4f rotMat = getRotationMatrix(rot);
		v1 = rotMat * v1;
		v2 = rotMat * v2;
		v3 = rotMat * v3;
		v4 = rotMat * v4;
	}

	Vec4f translation(x + ox, y + oy, 0, 0);
	v1 = v1 + translation;
	v2 = v2 + translation;
	v3 = v3 + translation;
	v4 = v4 + translation;

	m_positionBuffer.data(v1.getX(), v1.getY());
	m_positionBuffer.data(v2.getX(), v2.getY());
	m_positionBuffer.data(v3.getX(), v3.getY());
	m_positionBuffer.data(v4.getX(), v4.getY());

	for (int i = 0; i < 4;i++)
		m_colorBuffer.data(m_tint);
	
	m_textureCoordBuffer.data(texture.m_x, texture.m_y);
	m_textureCoordBuffer.data(texture.m_x, texture.m_y + texture.m_h);
	m_textureCoordBuffer.data(texture.m_x + texture.m_w, texture.m_y + texture.m_h);
	m_textureCoordBuffer.data(texture.m_x + texture.m_w, texture.m_y);

	m_vertices += 4;
}

bool SpriteBatch::hasBegan()
{
	return began;
}

int SpriteBatch::getMaxVertices()
{
	return m_maxVertices;
}

int SpriteBatch::getVertexCount()
{
	return m_vertices;
}

Mat4f& SpriteBatch::getViewProjectionMatrix()
{
	return m_viewProjectionMatrix;
}

Color& SpriteBatch::getTint()
{
	return m_tint;
}

void SpriteBatch::setViewProjectionMatrix(const Mat4f& val)
{
	m_viewProjectionMatrix = val;
}

void SpriteBatch::setTint(const Color& tint)
{
	m_tint = tint;
}