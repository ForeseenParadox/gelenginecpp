#pragma once
#include "texture.h"
#include "vbo.h"
#include "vao.h"
#include "math.h"
#include "shader.h"
#include "color.h"

class SpriteBatch
{
private:
	Texture* m_texture;
	ShaderProgram m_shader;
	VertexBuffer m_positionBuffer, m_colorBuffer, m_textureCoordBuffer;
	VertexArray m_vao;
	Mat4f m_viewProjectionMatrix;
	Color m_tint;
	int m_maxVertices;
	int m_vertices;
	bool began;
	void flush();
	void init(int);
public:
	SpriteBatch();
	SpriteBatch(int maxVertices);
	~SpriteBatch();
	void begin();
	void end();

	void render(const TextureRegion&, float, float); // x, y
	void render(const TextureRegion&, float, float, float);// ox,oy,x,y,r
	void render(const TextureRegion&, float, float, float, float, float);// ox,oy,x,y,r
	void render(const TextureRegion&, float, float, float, float, float, float, float); // ox,oy,x,y,r,sx,sy

	bool hasBegan();
	int getMaxVertices();
	int getVertexCount();
	Mat4f& getViewProjectionMatrix();
	Color& getTint();
	void setViewProjectionMatrix(const Mat4f&);
	void setTint(const Color&);
};