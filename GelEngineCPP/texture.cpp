#include "texture.h"
#include <GL\glew.h>

void unbindTexture()
{
	glBindTexture(GL_TEXTURE_2D, 0);
}

Texture::Texture()
{
	glGenTextures(1, &m_handle);
}

Texture::Texture(const std::string& path)
{
	glGenTextures(1, &m_handle);
	load(Bitmap(path));
}

Texture::Texture(const Bitmap& bitmap)
{
	glGenTextures(1, &m_handle);
	load(bitmap);
}

Texture::~Texture()
{
	glDeleteTextures(1, &m_handle);
}

GLuint Texture::getHandle() const
{
	return m_handle;
}

int Texture::getWidth() const
{
	return m_w;
}

int Texture::getHeight() const
{
	return m_h;
}

void Texture::bind() const
{
	bind(0);
}

void Texture::bind(unsigned int textureUnit) const
{
	glActiveTexture(GL_TEXTURE0 + textureUnit);
	glBindTexture(GL_TEXTURE_2D, m_handle);
}

void Texture::load(const std::string& path)
{
	load(Bitmap(path));
}

void Texture::load(const Bitmap& bitmap)
{
	bind();

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);

	if (bitmap.hasAlphaChannel())
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, bitmap.getWidth(), bitmap.getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, bitmap.getData());
	} else
	{
		glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, bitmap.getWidth(), bitmap.getHeight(), 0, GL_RGB, GL_UNSIGNED_BYTE, bitmap.getData());
	}
	m_w = bitmap.getWidth();
	m_h = bitmap.getHeight();
}