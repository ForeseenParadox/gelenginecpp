#pragma once
#include "bitmap.h"
#include <string>
#include <GL\glew.h>

/* Unbinds the texture bound to the current active texture unit. */
void unbindTexture();

/* An OpenGL texture that can be rendered to an OpenGL context. */
class Texture
{
private:
	/* The OpenGL handle to the texture. */
	GLuint m_handle;
	int m_w, m_h;
public:
	/* Constructs a new texture, but does not load any data. */
	Texture();
	/* Constructs a new texture and loads it with the data in the specified bitmap */
	Texture(const Bitmap&);
	/* Constructs a new texture and loads it with the data retrieved from the image specified by the path */
	Texture(const std::string&);
	/* Destructs this texture and releases all resources associated with it. */
	~Texture();
	/* @return the OpenGL handle of this texture */
	GLuint getHandle() const;
	/* @return the width of the texture */
	int getWidth() const;
	/* @return the height of the texture */
	int getHeight() const;
	/* Binds this texture to texure unit 0 for use. */
	void bind() const;
	/* Binds this texture to the specified texture unit for use. */
	void bind(unsigned int) const;
	/* Loads a texture with the data stored in the image specified by the path. */
	void load(const std::string& path);
	/* Loads this texture with the data stored in the specified bitmap. */
	void load(const Bitmap&);
};

struct TextureRegion
{
public:
	/* A reference to the texture of this texture region. */
	const Texture& m_texture;
	/* The components of this texture region. */
	float m_x, m_y, m_w, m_h;

	/* Creates a new texture region with the specified texture and normalized components. */
	TextureRegion(const Texture& texture, float x, float y, float w, float h) :
		m_texture(texture)
	{
		m_x = x;
		m_y = y;
		m_w = w;
		m_h = h;
	}

	/* Creates a texture region with the specified texture and unnormalized coordinates. */
	TextureRegion(const Texture& texture, int x, int y, int w, int h) :
		m_texture(texture)
	{
		m_x = (float)x / texture.getWidth();
		m_y = (float)y / texture.getHeight();
		m_w = (float)w / texture.getWidth();
		m_h = (float)h / texture.getHeight();
	}

	float getUnnormalizedWidth() const
	{
		return m_texture.getWidth() * m_w;
	}

	float getUnnormalizedHeight() const
	{
		return m_texture.getHeight() * m_h;
	}
};