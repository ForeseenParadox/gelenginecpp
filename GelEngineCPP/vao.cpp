#include "vao.h"
#include <iostream>

VertexArray::VertexArray()
{
	glGenVertexArrays(1, &m_handle);
}

VertexArray::~VertexArray()
{
	glDeleteVertexArrays(1, &m_handle);
}

GLuint VertexArray::getHandle() const
{
	return m_handle;
}

void VertexArray::bind() const
{
	glBindVertexArray(m_handle);
}

void VertexArray::update(VertexBuffer& buffer, int attribIndex, int elementCount)
{
	update(buffer, attribIndex,elementCount,0,0);
}

void VertexArray::update(VertexBuffer& buffer, int attribIndex, int elementCount, int stride, int offset)
{
	bind();

	buffer.bind();
	glEnableVertexAttribArray(attribIndex);
	glVertexAttribPointer(attribIndex, elementCount, GL_FLOAT, GL_FALSE, stride, reinterpret_cast<void*> (offset));

	unbindVAO();
}

void VertexArray::render(GLenum mode, int indices)
{
	bind();
	glDrawArrays(mode, 0, indices);
	unbindVAO();
}

void unbindVAO()
{
	glBindVertexArray(0);
}