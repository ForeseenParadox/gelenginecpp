#pragma once

#include <GL\glew.h>
#include "vbo.h"
#include <vector>

/* Unbinds the current bound VAO. */
void unbindVAO();

/* The count of elemements in one position attribute */
const int POS_ELEMENTS = 2;
/* The count of elements in one color attribute. */
const int COLOR_ELEMENTS = 4;
/* The count of elements in one texture coordinate attribute. */
const int TEX_COORD_ELEMENTS = 2;
/* The total count of elements in a vertex. */
const int VERT_ELEMENTS = POS_ELEMENTS + COLOR_ELEMENTS + TEX_COORD_ELEMENTS;

/* An OpenGL vertex array object which is used to render vertices. A vertex array object is an object 
which specifies the format of data stored in vertex buffer objects.*/
class VertexArray
{
private:
	/* The OpenGL handle for this vertex array. */
	GLuint m_handle;
public:
	/* Constructs a new vertex array. */
	VertexArray();
	/* Destory the vertex array and free resources. */
	~VertexArray();
	/* @return the OpenGL handle of this vertex array object.*/
	GLuint getHandle() const;
	/* Binds this VAO to be used. */
	void bind() const;
	/* Updates this VAO using zero stride and offset. */
	void update(VertexBuffer&, int, int);
	/* Updates the storage format of the attributes referenced by the specified vertex buffer. 
	This VAO will reference the data of the specified vbos, so it is expected their data buffer 
	has already been filled with the data planned to be sent through the OpenGL pipeline. */
	void update(VertexBuffer&, int, int, int, int);
	/* Renders this VAO. This means sending the data, referenced by the VBO specified in updateVertexAttribs(), through 
	the OpenGL pipeline.*/
	void render(GLenum, int);
};