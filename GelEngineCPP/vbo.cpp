#include "vbo.h"
#include <iostream>

VertexBuffer::VertexBuffer(int bufSize)
{
	glGenBuffers(1, &m_handle);
	m_bufferSize = bufSize;
	m_buffer = new float[m_bufferSize];
	m_pos = 0;
}

VertexBuffer::~VertexBuffer()
{
	glDeleteBuffers(1, &m_handle);
	delete[] m_buffer;
}

GLuint VertexBuffer::getHandle() const
{
	return m_handle;
}

int VertexBuffer::getPosition() const
{
	return m_pos;
}

void VertexBuffer::bind() const
{
	glBindBuffer(GL_ARRAY_BUFFER, m_handle);
}

VertexBuffer* VertexBuffer::data(float dat)
{
	if (check(1))
	{
		m_buffer[m_pos++] = dat;
	}
	return this;
}

VertexBuffer* VertexBuffer::data(float dat0, float dat1)
{

	return data(dat0)->data(dat1);
	/*if (check(2))
	{
		m_buffer[m_pos++] = dat0;
		m_buffer[m_pos++] = dat1;
	}
	return this;*/
}

VertexBuffer* VertexBuffer::data(float dat0, float dat1, float dat2)
{
	if (check(3))
	{
		m_buffer[m_pos++] = dat0;
		m_buffer[m_pos++] = dat1;
		m_buffer[m_pos++] = dat2;
	}
	return this;
}

VertexBuffer* VertexBuffer::data(float dat0, float dat1, float dat2, float dat3)
{
	if (check(4))
	{
		m_buffer[m_pos++] = dat0;
		m_buffer[m_pos++] = dat1;
		m_buffer[m_pos++] = dat2;
		m_buffer[m_pos++] = dat3;
	}
	return this;
}

VertexBuffer* VertexBuffer::data(const Vec2f& dat)
{
	if (check(2))
	{
		m_buffer[m_pos++] = dat.getX();
		m_buffer[m_pos++] = dat.getY();
	}
	return this;
}

VertexBuffer* VertexBuffer::data(const Vec3f& dat)
{
	if (check(3))
	{
		m_buffer[m_pos++] = dat.getX();
		m_buffer[m_pos++] = dat.getY();
		m_buffer[m_pos++] = dat.getZ();
	}
	return this;
}

VertexBuffer* VertexBuffer::data(const Vec4f& dat)
{
	if (check(4))
	{
		m_buffer[m_pos++] = dat.getX();
		m_buffer[m_pos++] = dat.getY();
		m_buffer[m_pos++] = dat.getZ();
		m_buffer[m_pos++] = dat.getW();
	}
	return this;
}

VertexBuffer* VertexBuffer::data(const Color& dat)
{
	if (check(4))
	{
		m_buffer[m_pos++] = dat.getRed();
		m_buffer[m_pos++] = dat.getGreen();
		m_buffer[m_pos++] = dat.getBlue();
		m_buffer[m_pos++] = dat.getAlpha();
	}
	return this;
}

float* VertexBuffer::getData() const
{
	return m_buffer;
}

void VertexBuffer::sendData()
{
	bind();
	glBufferData(GL_ARRAY_BUFFER, sizeof(float) * m_pos, m_buffer, GL_STREAM_DRAW);
}

void VertexBuffer::clearBuffer()
{
	m_pos = 0;
}

bool VertexBuffer::check(int count)
{
	return m_pos + count <= m_bufferSize;
}

void unbindVBO()
{
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}