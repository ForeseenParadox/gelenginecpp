#pragma once

#include <GL\glew.h>
#include "math.h"
#include "color.h"

/* Unbinds the currently bound vertex buffer object. */
void unbindVBO();

/* An OpenGL vertex buffer that stores raw vertex data.*/
class VertexBuffer
{
private:
	/* The OpenGL handle for this vertex buffer object. */
	GLuint m_handle;
	/* A pointer to the first element of the vertex buffer. */
	float* m_buffer;
	/* The size allocated to this vertex buffer object. */
	int m_bufferSize;
	/* The current position of this vertex buffer object. */
	int m_pos;
	/* Checks if there is any more space to put the specified amount of elements into this buffer. */
	bool check(int);
public:
	/* Constructs a new vertex buffer object with the specified data store.*/
	VertexBuffer(int);
	/* Delete the vertex buffer and free resources. */
	~VertexBuffer();
	/* @return the OpenGL handle of this vertex buffer object. */
	GLuint getHandle() const;
	/* @return the size allocated to this vertex buffer object.*/
	int getBufferSize() const;
	/* @return the current position of this vertex buffer object. */
	int getPosition() const;
	/* Binds this vertex buffer to be used. */
	void bind() const;
	/* Sends 1 float to this vertex buffer's data store. */
	VertexBuffer* data(float);
	/* Sends 2 floats to this vertex buffer's data store. */
	VertexBuffer* data(float, float);
	/* Sends 3 floats to this vertex buffer's data store. */
	VertexBuffer* data(float, float, float);
	/* Sends 4 floats to this vertex buffer's data store. */
	VertexBuffer* data(float, float, float, float);
	/* Sends 1 vec2f to this vertex buffer's data store. */
	VertexBuffer* data(const Vec2f&);
	/* Sends 1 vec3f to this vertex buffer's data store. */
	VertexBuffer* data(const Vec3f&);
	/* Sends 1 vec4f to this vertex buffer's data store. */
	VertexBuffer* data(const Vec4f&);
	/* Sends 1 color to this vertex buffer's data store. */
	VertexBuffer* data(const Color&);
	/* @return the raw data of this vertex buffer */
	float* getData() const;
	/* Sends the data in this buffer to the GPU. */
	void sendData();
	/* Clears this buffer's data by resetting the position. */
	void clearBuffer();
};