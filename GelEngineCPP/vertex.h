#pragma once
#include "math.h"
#include "color.h"

/* A struct representing a vertex with multiple attributes such as position, color and texture coordinates. */
struct Vertex
{
	/* The 2d position of this vertex. */
	Vec2f m_pos;
	/* The RGBA color of this vertex. */
	Color m_col;
	/* The texture coordinate of this vertex. */
	Vec2f m_texCoord;
	/* Constructs a vertex with all default attributes. */
	Vertex();
	/* Constructs a vertex with the specified position with the rest of the attributes set to default values. */
	Vertex(const Vec2f&);
	/* Constructs a vertex with the specified position and color with the rest of the attributes set to default values. */
	Vertex(const Vec2f&, Color&);
	/* Construts a vertex with all of the specified attributes. */
	Vertex(const Vec2f&, Color&, Vec2f&);
};