#include "window.h"

Window::Window(const std::string& title, int width, int height, bool resizable, bool vsync)
{
	Uint32 flags = SDL_WINDOW_OPENGL;

	if (resizable)
		flags |= SDL_WINDOW_RESIZABLE;

	m_window = SDL_CreateWindow(title.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, width, height, flags);
	m_glContext = SDL_GL_CreateContext(m_window);

	GLenum glewError = glewInit();
	if (glewError != GLEW_OK)
	{
		std::cerr << glewGetErrorString(glewError) << std::endl;
	}

	if (vsync)
	{
		SDL_GL_SetSwapInterval(1);
	} else
	{
		SDL_GL_SetSwapInterval(0);
	}
}

Window::~Window()
{
	SDL_GL_DeleteContext(m_glContext);
	SDL_DestroyWindow(m_window);
}

const char* Window::getTitle() const
{
	return SDL_GetWindowTitle(m_window);
}

int Window::getWidth() const
{
	int w;
	SDL_GetWindowSize(m_window, &w, nullptr);
	return w;
}

int Window::getHeight() const
{
	int h;
	SDL_GetWindowSize(m_window, nullptr, &h);
	return h;
}

void Window::update()
{
	SDL_GL_SwapWindow(m_window);
}

void Window::setTitle(const std::string& title)
{
	SDL_SetWindowTitle(m_window, title.c_str());
}

void Window::setWidth(int width)
{
	setSize(width, getHeight());
}

void Window::setHeight(int height)
{
	setSize(getWidth(), height);
}

void Window::setSize(int width, int height)
{
	SDL_SetWindowSize(m_window, width, height);
}
