#pragma once
#include <SDL\SDL.h>
#include <GL\glew.h>
#include <string>
#include <iostream>

/* A window with an OpenGL context. */
class Window
{
private:
	/* A pointer to the SDL window. */
	SDL_Window* m_window;
	/* The SDL OpenGL context of this window. */
	SDL_GLContext m_glContext;
public:
	/* Creates a window with the specified attributes. */
	Window(const std::string& title, int width, int height, bool resizable, bool vsync);
	/* Destructs this window and frees the gl context and window resources. */
	~Window();
	/* @return the title of this window. */
	const char* getTitle() const;
	/* @return the width of this window. */
	int getWidth() const;
	/* @return the height of this window. */
	int getHeight() const;
	/* Sets the title of this window. */
	void setTitle(const std::string&);
	/* Sets the width of this window. */
	void setWidth(int);
	/* Sets the height of this window. */
	void setHeight(int);
	/* Sets the size of this window. */
	void setSize(int, int);
	/* Updates the buffering of this window to display the newly rendered contents. */
	void update();
};

